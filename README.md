# Online Learning

Application for Online Learning

## Installation

1. Clone this repo
2. `cd online-learning`
3. `composer install`
4. `cp .env.example .env`
5. `php artisan key:generate`
6. Update `.env` file (Name, URL, Database, etc)
7. `php artisan migrate`
8. `php artisan serve`