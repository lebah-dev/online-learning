<?php

use App\Http\Controllers\ClassRoomCodeController;
use App\Http\Controllers\ClassRoomController;
use App\Http\Controllers\ClassRoomPostController;
use App\Http\Controllers\ClassRoomResourceController;
use App\Http\Controllers\ClassRoomScoreController;
use App\Http\Controllers\ClassRoomStudentController;
use App\Http\Controllers\ClassRoomTaskController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', 'dashboard');

Route::middleware('auth')->group(function () {
    Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    Route::view('class-rooms/create', 'class-rooms.create')->name('class-rooms.create');
    Route::post('class-rooms', [ClassRoomController::class, 'store'])->name('class-rooms.store');
    Route::get('class-rooms/{classRoomSlug}', [ClassRoomController::class, 'detail'])->name('class-rooms.detail');
    Route::get('class-rooms/{classRoomSlug}/edit', [ClassRoomController::class, 'edit'])->name('class-rooms.edit');
    Route::post('class-rooms/{classRoomSlug}', [ClassRoomController::class, 'update'])->name('class-rooms.update');
    Route::delete('class-rooms/{classRoomSlug}/archive', [ClassRoomController::class, 'archive'])->name('class-rooms.archive');

    Route::get('archived-class-rooms', [ClassRoomController::class, 'archived'])->name('class-rooms.archived');
    Route::put('archived-class-rooms/{classRoomSlug}/restore', [ClassRoomController::class, 'restore'])->name('class-rooms.restore');

    Route::post('class-room-code/generate', [ClassRoomCodeController::class, 'generate'])->name('class-room-code.generate');
    Route::post('class-room-code/delete', [ClassRoomCodeController::class, 'delete'])->name('class-room-code.delete');
    Route::post('class-room-code/join', [ClassRoomCodeController::class, 'join'])->name('class-room-code.join');

    Route::get('class-rooms/{classRoomSlug}/students', [ClassRoomStudentController::class, 'index'])->name('class-rooms.students.index');
    Route::post('class-rooms/{classRoomSlug}/students', [ClassRoomStudentController::class, 'invite'])->name('class-rooms.students.invite');
    Route::delete('class-rooms/{classRoomSlug}/students', [ClassRoomStudentController::class, 'detach'])->name('class-rooms.students.detach');
    Route::get('class-rooms/{classRoomSlug}/students/{encryptedId}', [ClassRoomStudentController::class, 'detail'])->name('class-rooms.students.detail');

    Route::get('class-rooms/{classRoomSlug}/posts', [ClassRoomPostController::class, 'index'])->name('class-rooms.posts.index');
    Route::get('class-rooms/{classRoomSlug}/posts/create', [ClassRoomPostController::class, 'create'])->name('class-rooms.posts.create');
    Route::post('class-rooms/{classRoomSlug}/posts', [ClassRoomPostController::class, 'store'])->name('class-rooms.posts.store');
    Route::get('class-rooms/{classRoomSlug}/posts/{postSlug}', [ClassRoomPostController::class, 'detail'])->name('class-rooms.posts.detail');
    Route::get('class-rooms/{classRoomSlug}/posts/{postSlug}/edit', [ClassRoomPostController::class, 'edit'])->name('class-rooms.posts.edit');
    Route::post('class-rooms/{classRoomSlug}/posts/{postSlug}', [ClassRoomPostController::class, 'update'])->name('class-rooms.posts.update');
    Route::delete('class-rooms/{classRoomSlug}/posts/{postSlug}', [ClassRoomPostController::class, 'delete'])->name('class-rooms.posts.delete');

    Route::get('class-rooms/{classRoomSlug}/resources', [ClassRoomResourceController::class, 'index'])->name('class-rooms.resources.index');
    Route::post('class-rooms/{classRoomSlug}/resources', [ClassRoomResourceController::class, 'create'])->name('class-rooms.resources.create');
    Route::get('class-rooms/{classRoomSlug}/resources/{resourceSlug}', [ClassRoomResourceController::class, 'detail'])->name('class-rooms.resources.detail');
    Route::get('class-rooms/{classRoomSlug}/resources/{resourceSlug}/full-screen', [ClassRoomResourceController::class, 'fullScreen'])->name('class-rooms.resources.full-screen');
    Route::delete('class-rooms/{classRoomSlug}/resources/{resourceSlug}', [ClassRoomResourceController::class, 'delete'])->name('class-rooms.resources.delete');

    Route::get('class-rooms/{classRoomSlug}/tasks', [ClassRoomTaskController::class, 'index'])->name('class-rooms.tasks.index');
    Route::get('class-rooms/{classRoomSlug}/tasks/create', [ClassRoomTaskController::class, 'create'])->name('class-rooms.tasks.create');
    Route::post('class-rooms/{classRoomSlug}/tasks', [ClassRoomTaskController::class, 'store'])->name('class-rooms.tasks.store');
    Route::get('class-rooms/{classRoomSlug}/tasks/{taskSlug}', [ClassRoomTaskController::class, 'detail'])->name('class-rooms.tasks.detail');
    Route::get('class-rooms/{classRoomSlug}/tasks/{taskSlug}/edit', [ClassRoomTaskController::class, 'edit'])->name('class-rooms.tasks.edit');
    Route::post('class-rooms/{classRoomSlug}/tasks/{taskSlug}/update', [ClassRoomTaskController::class, 'update'])->name('class-rooms.tasks.update');
    Route::delete('class-rooms/{classRoomSlug}/tasks/{taskSlug}', [ClassRoomTaskController::class, 'delete'])->name('class-rooms.tasks.delete');

    Route::post('class-rooms/{classRoomSlug}/tasks/{taskSlug}/submit', [ClassRoomTaskController::class, 'submit'])->name('class-rooms.tasks.submit');
    Route::post('class-rooms/{classRoomSlug}/tasks/{taskSlug}/score', [ClassRoomTaskController::class, 'score'])->name('class-rooms.tasks.score');

    Route::get('class-rooms/{classRoomSlug}/score', [ClassRoomScoreController::class, 'index'])->name('class-rooms.score.index');

    Route::get('profile', [ProfileController::class, 'profile'])->name('profile');
    Route::post('profile', [ProfileController::class, 'update'])->name('profile.update');
});

Route::get('retrive-storage/{encryptedPath}', function ($encryptedPath) {
    $path = decrypt($encryptedPath);
    if (!Storage::exists($path)) {
        return null;
    }

    $content = Storage::get($path);

    return Response::make($content, 200, [
        'Content-Type' => Storage::mimeType($path),
        'Content-Disposition' => 'inline; filename="'. basename($path) .'"'
    ]);
})->name('retrive-storage');

Route::get('download-storage/{encryptedPath}', function ($encryptedPath) {
    return Storage::download(decrypt($encryptedPath));
})->name('download-storage');