<div class="main-sidebar sidebar-style-2">
	<aside id="sidebar-wrapper">
		<div class="sidebar-brand mb-5">
			<img src="{{ asset('images/logo.png') }}" alt="Xin Yuan" class="w-75" style="object-fit: scale-down; object-position: center;"> 
	 </div>
	 <div class="sidebar-brand sidebar-brand-sm">
			<img src="{{ asset('images/logo.png') }}" alt="Xin Yuan" class="w-100" style="object-fit: scale-down; object-position: center;"> 
	 </div>
		<ul class="sidebar-menu">
			<li><a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-home"></i> <span>Home</span></a></li>

			@foreach($classRooms as $classRoom)
			<li class="nav-item dropdown {{ request('classRoomSlug') == $classRoom->slug ? 'active' : '' }}">
				<a href="#" class="nav-link has-dropdown">
					<i class="fas fa-chalkboard"></i>
					<span class="text-truncate">{{ $classRoom->name }}</span>
				</a>
				<ul class="dropdown-menu">
					<li class="{{ (request()->routeIs('class-rooms.detail') && request('classRoomSlug') == $classRoom->slug) ? 'active' : '' }}">
						<a class="nav-link" href="{{ route('class-rooms.detail', $classRoom->slug) }}">
							<i class="fas fa-columns"></i>
							<span>Dashboard</span>
						</a>
					</li>
					<li class="{{ (request()->routeIs('class-rooms.students*') && request('classRoomSlug') == $classRoom->slug) ? 'active' : '' }}">
						<a class="nav-link" href="{{ route('class-rooms.students.index', $classRoom->slug) }}">
							<i class="fas fa-users"></i> <span>Students</span>
						</a>
					</li>
					<li class="{{ (request()->routeIs('class-rooms.posts*') && request('classRoomSlug') == $classRoom->slug) ? 'active' : '' }}">
						<a class="nav-link" href="{{ route('class-rooms.posts.index', $classRoom->slug) }}">
							<i class="fas fa-newspaper"></i> <span>Posts</span>
						</a>
					</li>
					<li class="{{ (request()->routeIs('class-rooms.resources*') && request('classRoomSlug') == $classRoom->slug) ? 'active' : '' }}">
						<a class="nav-link" href="{{ route('class-rooms.resources.index', $classRoom->slug) }}">
							<i class="fas fa-book-open"></i> <span>Resources</span>
						</a>
					</li>
					<li class="{{ (request()->routeIs('class-rooms.tasks*') && request('classRoomSlug') == $classRoom->slug) ? 'active' : '' }}">
						<a class="nav-link" href="{{ route('class-rooms.tasks.index', $classRoom->slug) }}">
							<i class="fas fa-tasks"></i> <span>Tasks / Exams</span>
						</a>
					</li>
					<li class="{{ (request()->routeIs('class-rooms.score*') && request('classRoomSlug') == $classRoom->slug) ? 'active' : '' }}">
						<a class="nav-link" href="{{ route('class-rooms.score.index', $classRoom->slug) }}">
							<i class="fas fa-star"></i> <span>Scores</span>
						</a>
					</li>
				</ul>
			</li>
			@endforeach
		</ul>
	</aside>
</div>