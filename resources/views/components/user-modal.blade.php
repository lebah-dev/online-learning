<div class="modal fade" tabindex="-1" role="dialog" id="detail{{ $student->id }}">
	<div class="modal-dialog mt-5" role="document">
		<div class="modal-content profile-widget">
			<div class="profile-widget-header">                     
				<img alt="image" src="{{ $student->avatar }}" class="rounded-circle profile-widget-picture">
				<div class="profile-widget-items">
					<div class="profile-widget-item">
						<div class="profile-widget-item-label">Tasks</div>
						<div class="profile-widget-item-value text-nowrap">
							<span class="text-success">{{ $student->submitedTasks()->where('class_room_id', $classRoom->id)->count() }}</span>
							<span> / </span>
							<span>{{ $classRoom->tasks()->count() }}</span>
						</div>
					</div>
					<div class="profile-widget-item">
						<div class="profile-widget-item-label">Score</div>
						<div class="profile-widget-item-value">
							<span class="text-primary">{{ $student->classRooms()->find($classRoom->id)->pivot->average_score }}</span>
						</div>
					</div>
				</div>
			</div>
			<div class="profile-widget-description pb-0">
				<div class="profile-widget-name">{{ $student->name }} <div class="text-muted d-inline font-weight-normal"><div class="slash"></div> {{ $student->email }}</div></div>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				consequat.</p>
			</div>
			<div class="card-footer bg-white text-center border-0 pt-0">
				<button class="btn btn-secondary" data-dismiss="modal" aria-label="Close">Close</button>
			</div>
		</div>
	</div>
</div>