<x-auth-layout>
	<x-slot name="title">Register</x-slot>
	<div class="card card-primary">
		<div class="card-header"><h4>Register</h4></div>

		<div class="card-body">
			<form method="POST" action="{{ route('register') }}" onkeydown="return event.key != 'Enter';"> @csrf
				<div class="form-group">
					<label for="name">Name</label>
					<input id="name" type="name" class="form-control @error('name') is-invalid @enderror" name="name" tabindex="1" autofocus>
					@error('name') <div class="invalid-feedback">{{ __($message) }}</div> @enderror
				</div>

				<div class="form-group">
					<label for="email">Email</label>
					<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" tabindex="1" autofocus>
					@error('email') <div class="invalid-feedback">{{ __($message) }}</div> @enderror
				</div>

				<div class="form-group">
					<label for="password">Password</label>
					<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" tabindex="1" autofocus>
					@error('password') <div class="invalid-feedback">{{ __($message) }}</div> @enderror
				</div>

				<div class="form-group">
					<label for="password_confirmation">Password Confirmation</label>
					<input id="password_confirmation" type="password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation" tabindex="1" autofocus>
					@error('password_confirmation') <div class="invalid-feedback">{{ __($message) }}</div> @enderror
				</div>

				<div class="mb-4 text-center">Register As</div>

				<div class="form-group row">
					<div class="col-lg-6">
						<button type="submit" name="role" value="student" class="btn btn-primary btn-lg btn-block">
							Student
						</button>
					</div>
					<div class="col-lg-6">
						<button type="submit" name="role" value="teacher" class="btn btn-primary btn-lg btn-block">
							Teacher
						</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</x-auth-layout>