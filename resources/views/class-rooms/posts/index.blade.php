<x-app-layout>
	<x-slot name="header">{{ $classRoom->name }} Posts</x-slot>

	<div class="row">
		@foreach($classRoom->posts as $post)
			<div class="col-lg-4 mb-4">
				<article class="article article-style-b h-100">
					<div class="article-details">
						<div class="article-title text-truncate"><h2><a href="{{ route('class-rooms.posts.detail', [$classRoom->slug, $post->slug]) }}">{{ $post->title }}</a></h2></div>
						<div class="text-small article-category">{{ $post->created_at->diffForHumans() }}</div>
						<div>{!! $post->content_preview !!}</div>
					</div>
				</article>
			</div>
		@endforeach
		@if(auth()->user()->roleIs('teacher'))
		<div class="col-lg-4 mb-4">
			<div class="card bg-transparent shadow-none border h-100">
				<div class="card-body d-flex align-items-center justify-content-center">
					<a href="{{ route('class-rooms.posts.create', $classRoom->slug) }}" class="btn btn-secondary btn-icon icon-left"><i class="fas fa-plus"></i> Create New Post</a>
				</div>
			</div>
		</div>
		@endif
	</div>
</x-app-layout>