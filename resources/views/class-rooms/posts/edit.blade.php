<x-app-layout>
	<x-slot name="header">Edit Class Room Post</x-slot>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<form action="{{ route('class-rooms.posts.update', [$classRoom->slug, $post->slug]) }}" method="post" enctype="multipart/form-data" autocomplete="off">
					@csrf

					<div class="card-header">
						<h4>Class Room's Post Form</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label>Title</label>
							<input id="title" type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $post->title }}" onkeyup="$('#slug').val(slugify($(this).val()))">
							@error('title') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label>Slug</label>
							<input id="slug" type="text" name="slug" class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') ?? $post->slug }}">
							@error('slug') <div class="invalid-feedback">{{ $message }}</div> @enderror
							<div class="text-small text-muted">Used to identify the class room. It must be unique.</div>
						</div>
						<div class="form-group mb-0">
							<label>Content</label>
							<textarea name="content" class="form-control summernote @error('content') is-invalid @enderror" rows="10">{{ old('content') ?? $post->content }}</textarea>
							@error('content') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="card-footer text-right">
						<button class="btn btn-icon icon-left btn-primary"><i class="fas fa-save"></i> Save</button>
						<a href="{{ route('dashboard') }}" class="btn btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	@push('css-libraries')
		<link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
	@endpush

	@push('js-libraries')
		<script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
	@endpush
</x-app-layout>