<x-app-layout>
	<x-slot name="header">{{ $post->title }}</x-slot>
	<div class="row">
		<div class="col-lg-12">
			@if(auth()->user()->roleIs('teacher'))
			<div class="d-flex justify-content-end mb-2">
				<a href="{{ route('class-rooms.posts.edit', [$classRoom->slug, $post->slug]) }}" class="btn btn-warning btn-icon icon-left"><i class="fas fa-edit"></i> Edit Post</a>
				<button type="button" class="btn btn-danger btn-icon icon-left ml-2" data-confirm="Delete Post?|You are about to delete a post. This action can not be undone. Do you still want to continue?" data-confirm-yes="$('#deletePost').submit();"><i class="fas fa-trash"></i> Delete Post</button>
				<form id="deletePost" action="{{ route('class-rooms.posts.delete', [$classRoom->slug, $post->slug]) }}" method="post" class="hidden">@csrf @method('delete')</form>
			</div>
			@endif
			<article class="card">
				<div class="card-body">
					<div class="mb-2 font-weight-bold text-small"><a href="{{ route('class-rooms.posts.index', $classRoom->slug) }}">◀ Back</a> &bull; <a>{{ $classRoom->teacher->name }}</a> &bull; <a>{{ $post->published_at->diffForHumans() }}</a></div>
					<div>{!! $post->content !!}</div>
				</div>
			</article>
		</div>
	</div>
</x-app-layout>