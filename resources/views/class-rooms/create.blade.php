<x-app-layout>
	<x-slot name="header">Create Class Room</x-slot>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<form action="{{ route('class-rooms.store') }}" method="post" enctype="multipart/form-data" autocomplete="off">
					@csrf

					<div class="card-header">
						<h4>Class Room Form</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label>Cover Image</label>
							<input type="file" name="cover" class="form-control @error('cover') is-invalid @enderror" value="{{ old('cover') }}">
							@error('cover') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}" onkeyup="$('#slug').val(slugify($(this).val()))">
							@error('name') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label>Slug</label>
							<input id="slug" type="text" name="slug" class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') }}">
							@error('slug') <div class="invalid-feedback">{{ $message }}</div> @enderror
							<div class="text-small text-muted">Used to identify the class room. It must be unique.</div>
						</div>
						<div class="form-group mb-0">
							<label>Description</label>
							<textarea name="description" class="form-control summernote-simple @error('description') is-invalid @enderror" rows="10">{{ old('description') }}</textarea>
							@error('description') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="card-footer text-right">
						<button class="btn btn-icon icon-left btn-primary"><i class="fas fa-save"></i> Submit</button>
						<a href="{{ route('dashboard') }}" class="btn btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	@push('css-libraries')
		<link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
	@endpush

	@push('js-libraries')
		<script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
	@endpush
</x-app-layout>