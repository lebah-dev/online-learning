<x-app-layout>
	<x-slot name="header">{{ $classRoom->name }} Resources</x-slot>

	<div class="row">
		@foreach($classRoom->resources->sortBy('title') as $resource)
		<div class="col-lg-4 mb-4">
			<div class="card card-primary h-100">
				<div class="card-body d-flex align-items-center justify-content-center">
					<div class="text-center">
						<a href="{{ route('class-rooms.resources.detail', [$classRoom->slug, $resource->slug]) }}"><h6>{{ $resource->title}}</h6></a>
						<p class="mb-0 text-muted text-small">{{ $resource->type }} &bull; {{ $resource->created_at->diffForHumans() }}</p>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		@if(auth()->user()->roleIs('teacher'))
		<div class="col-lg-4 mb-4">
			<div class="card bg-transparent border shadow-none h-100">
				<div class="card-body d-flex align-items-center justify-content-center">
					<button type="button" class="btn btn-secondary btn-icon icon-left" data-toggle="modal" data-target="#addResource"><i class="fas fa-plus"></i> Add Resource</button>
				</div>
			</div>
		</div>
		@endif
	</div>

	@push('modals')
	<div class="modal fade" tabindex="-1" role="dialog" id="addResource">
		<div class="modal-dialog" role="document">
			<form action="" method="post" enctype="multipart/form-data">
				@csrf
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Add Resource</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>File</label>
							<input type="file" name="file" value="{{ old('file') }}" class="form-control @error('file') is-invalid @enderror">
							@error('file') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label>Title</label>
							<input type="text" name="title" value="{{ old('title') }}" class="form-control @error('title') is-invalid @enderror" onkeyup="$('#slug').val(slugify($(this).val()))">
							@error('title') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label>Slug</label>
							<input id="slug" type="text" name="slug" value="{{ old('slug') }}" class="form-control @error('slug') is-invalid @enderror">
							@error('slug') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="modal-footer bg-whitesmoke br">
						<button type="submit" class="btn btn-success btn-icon icon-left"><i class="far fa-plus"></i> Add Resource</button>
						<button type="button" class="btn btn-secondary btn-icon icon-left" data-dismiss="modal"><i class="far fa-times"></i> Cancel</button>
					</div>
				</div>
			</form>
		</div>
	</div>
	@endpush

	@if($errors->any())
	@push('scripts')
	<script>
		$(document).ready(function () {
			$('#addResource').modal('show');
		})
	</script>
	@endpush
	@endif
</x-app-layout>