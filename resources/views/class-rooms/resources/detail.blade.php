<x-app-layout>
	<x-slot name="header">{{ $resource->title }}</x-slot>

	<div class="row">
		<div class="col-lg-8">
			<div class="position-relative card">
				<div id="frame" class="position-relative w-100 overflow-hidden bg-secondary" style="padding-bottom: 150%"></div>
				<a href="{{ $resource->file }}#toolbar=0" target="_blank" class="position-absolute d-block m-2 btn btn-info shadow rounded" style="top: 0; right: 0;"><i class="fas fa-expand"></i></a>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="list-group">
				@foreach($classRoom->resources as $otherResource)
				<a href="{{ route('class-rooms.resources.detail', [$classRoom->slug, $otherResource->slug]) }}" class="list-group-item list-group-item-action border-0 {{ request('resourceSlug') == $otherResource->slug ? 'active' : '' }}">[{{ $otherResource->type }}] {{ $otherResource->title }}</a>
				@endforeach
			</div>
			@if(auth()->user()->roleIs('teacher'))
			<form action="{{ route('class-rooms.resources.delete', [$classRoom->slug, $resource->slug]) }}" method="post">
				@csrf @method('delete')
				<input type="hidden" name="resource_id">
				<button class="btn btn-block btn-danger btn-icon icon-left mt-4"><i class="fas fa-trash"></i> Delete</button>
			</form>
			@endif
		</div>
	</div>

	@push('scripts')
	<script>
		$(document).ready(function () {
			$('#frame').html(`<embed class="position-absolute w-100 h-100" type="application/pdf" src="{{ $resource->file }}#toolbar=0" id="pdf_content" style="pointer-events: none;" frameborder="0">`)
		})
	</script>
	@endpush
</x-app-layout>