<x-app-layout>
	<x-slot name="header">{{ $classRoom->name }} Students</x-slot>
	<div class="row">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
					<h4>Student List</h4>
					@if(auth()->user()->roleIs('teacher'))
					<div class="card-header-action">
						<button type="button" class="btn btn-icon icon-left btn-success" data-toggle="modal" data-target="#inviteStudentModal"><i class="fas fa-plus-circle"></i> Invite Student</button>
					</div>
					@endif
				</div>
				<div class="card-body">
					<ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
						@forelse($classRoom->students as $student)
						<li class="media">
							<img alt="image" class="mr-3 rounded-circle" height="50" width="50" src="{{ $student->avatar }}" style="object-fit: cover; object-position: center;">
							<div class="media-body">
								<div class="media-title">{{ $student->name }}</div>
								<div class="text-job text-muted">{{ $student->email }}</div>
							</div>
							<div class="media-items">
								<div class="media-item">
									<div class="media-value text-nowrap">
										<span class="text-success">{{ $student->submitedTasks()->where('class_room_id', $classRoom->id)->count() }}</span>
										<span> / </span>
										<span>{{ $classRoom->tasks()->count() }}</span>
									</div>
									<div class="media-label">Tasks</div>
								</div>
								<div class="media-item">
									<div class="media-value text-primary">{{ $student->pivot->average_score ?? '-' }}</div>
									<div class="media-label">Score</div>
								</div>
							</div>
							<div class="media-cta">
								<button type="button" class="btn btn-primary btn-icon icon-left" data-toggle="modal" data-target="#detail{{ $student->id }}"><i class="fas fa-eye"></i> Detail</button>
								@if(auth()->user()->roleIs('teacher'))
								<button type="button" class="btn btn-danger btn-icon icon-left" data-confirm="Detach Student?|You are about to detach a student from a class room. Do you still want to continue?" data-confirm-yes="$('#detach{{ $student->id }}').submit();"><i class="fas fa-trash"></i> Detach</button>
								<form id="detach{{ $student->id }}" action="{{ route('class-rooms.students.detach', $classRoom->slug) }}" method="post" class="hidden">
									@csrf @method('delete')
									<input type="hidden" name="student_id" value="{{ $student->id }}">
								</form>
								@endif
							</div>

							@push('modals')
							<x-user-modal :student="$student" :class-room="$classRoom"></x-user-modal>
							@endpush
						</li>
						@empty
						<li class="media">
							<div class="media-body">
								<div class="media-title">No student yet</div>
							</div>
						</li>
						@endforelse
					</ul>
				</div>
			</div>
		</div>
	</div>

	@if(auth()->user()->role == 'teacher')
	@push('modals')
	<div class="modal fade" tabindex="-1" role="dialog" id="inviteStudentModal">
		<form action="{{ route('class-rooms.students.invite', $classRoom->slug) }}" method="post">@csrf
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Invite Student</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>Email</label>
							<input type="email" name="student_email" value="{{ old('student_email') }}" placeholder="Student's Registered Email" class="form-control @error('student_email') is-invalid @enderror">
							@error('student_email') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="modal-footer bg-whitesmoke br">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Invite</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	@endpush

	@if($errors->get('student_email'))
	@push('scripts')
	<script>
		$(document).ready(function () {
			$('#inviteStudentModal').modal('show');
		})
	</script>
	@endpush
	@endif
	@endif
</x-app-layout>