<x-app-layout>
	<x-slot name="header">{{ $classRoom->name }} Score</x-slot>
	<div class="row">
		<div class="col-12">
			<div class="card card-primary">
				<div class="card-header">
					<h4>Student List</h4>
					<div class="card-header-action dropdown">
						<button type="button" data-toggle="dropdown" class="btn btn-danger dropdown-toggle" aria-expanded="false">Order By</button>
						<ul class="dropdown-menu dropdown-menu-sm dropdown-menu-right" x-placement="bottom-end" style="position: absolute; transform: translate3d(75px, 31px, 0px); top: 0px; left: 0px; will-change: transform;">
							<li><a href="{{ route(request()->route()->getName(), ['classRoomSlug' => $classRoom->slug, 'sort' => 'student_name', 'order' => 'asc']) }}" class="dropdown-item {{ request('sort') == 'student_name' && request('order') == 'asc' ? 'active' : '' }}">Student Name Ascending</a></li>
							<li><a href="{{ route(request()->route()->getName(), ['classRoomSlug' => $classRoom->slug, 'sort' => 'student_name', 'order' => 'desc']) }}" class="dropdown-item {{ request('sort') == 'student_name' && request('order') == 'desc' ? 'active' : '' }}">Student Name Descending</a></li>
							<li><a href="{{ route(request()->route()->getName(), ['classRoomSlug' => $classRoom->slug, 'sort' => 'average_score', 'order' => 'asc']) }}" class="dropdown-item {{ request('sort') == 'average_score' && request('order') == 'asc' ? 'active' : '' }}">Average Score Ascending</a></li>
							<li><a href="{{ route(request()->route()->getName(), ['classRoomSlug' => $classRoom->slug, 'sort' => 'average_score', 'order' => 'desc']) }}" class="dropdown-item {{ request('sort') == 'average_score' && request('order') == 'desc' ? 'active' : '' }}">Average Score Descending</a></li>
						</ul>
					</div>
				</div>
				<div class="card-body">
					<ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
						@forelse($classRoom->students()->sort(request())->get() as $student)
						<li class="media">
							<img alt="image" class="mr-3 rounded-circle" height="50" width="50" src="{{ $student->avatar }}" style="object-fit: cover; object-position: center;">
							<div class="media-body">
								<div class="media-title">{{ $student->name }}</div>
								<div class="text-job text-muted">{{ $student->email }}</div>
							</div>
							<div class="media-items">
								@foreach($classRoom->tasks as $index => $task)
								<div class="media-item" title="{{ $task->title }}">
									<div class="media-value">{{ $student->task($task->id)->score ?? '-' }}</div>
									<div class="media-label">Task {{ $index+1 }}</div>
								</div>
								@endforeach
								<div class="media-item" title="Average Score">
									<div class="media-value">{{ $student->averageScore($classRoom->id) }}</div>
									<div class="media-label">Average</div>
								</div>
							</div>
						</li>
						@empty
						<li class="media">
							<div class="media-body">
								<div class="media-title">No student yet</div>
							</div>
						</li>
						@endforelse
					</ul>
				</div>
			</div>
		</div>
	</div>

	@if(auth()->user()->role == 'teacher')
	@push('modals')
	<div class="modal fade" tabindex="-1" role="dialog" id="inviteStudentModal">
		<form action="{{ route('class-rooms.students.invite', $classRoom->slug) }}" method="post">@csrf
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Invite Student</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>Email</label>
							<input type="email" name="student_email" value="{{ old('student_email') }}" placeholder="Student's Registered Email" class="form-control @error('student_email') is-invalid @enderror">
							@error('student_email') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="modal-footer bg-whitesmoke br">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Invite</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	@endpush

	@if($errors->get('student_email'))
	@push('scripts')
	<script>
		$(document).ready(function () {
			$('#inviteStudentModal').modal('show');
		})
	</script>
	@endpush
	@endif
	@endif
</x-app-layout>