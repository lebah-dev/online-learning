<x-app-layout>
	<x-slot name="header">Archived Class Rooms</x-slot>
	<div class="row">
		@forelse($classRooms as $classRoom)
		<div class="col-lg-3">
			<div class="card card-secondary h-100">
				<div class="card-body d-flex flex-column align-items-stretch justify-content-between">
					<div>
						<h6 class="mb-4">{{ $classRoom->name }}</h6>
						<p class="mb-0"><i class="fas fa-users"></i> Students : 0</p>
						<p class="mb-0"><i class="fas fa-newspaper"></i> Posts : 0</p>
						<p class="mb-0"><i class="fas fa-book-open"></i> Resources : 0</p>
						<p class="mb-0"><i class="fas fa-tasks"></i> Task / Exam : 0</p>
					</div>
					<div>
						<button type="button" class="btn btn-sm btn-block btn-icon icon-left btn-outline-secondary" data-confirm="Restore Class Room?|You are about to restore a class room. Restored class room will be available and accessable through Home Page. Do you still want to continue?" data-confirm-yes="$('#restore-{{ $classRoom->slug }}').submit();"><i class="fas fa-eye"></i> Restore</button>
						<form id="restore-{{ $classRoom->slug }}" action="{{ route('class-rooms.restore', $classRoom->slug) }}" method="post" class="hidden">@csrf @method('put')</form>
					</div>
				</div>
			</div>
		</div>
		@empty
		<div class="col">
			<div class="card bg-transparent border shadow-none">
				<div class="card-body text-center">
					No class room has been archived yet.
				</div>
			</div>
		</div>
		@endforelse
	</div>
</x-app-layout>