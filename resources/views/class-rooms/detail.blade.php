<x-app-layout>
	<x-slot name="header">{{ $classRoom->name }}</x-slot>

	<div class="row">
		<div class="col-lg-7">
			<div class="row">
				@if(auth()->user()->role == 'teacher')
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header">
							<h4>Generate Invitation Code</h4>
							<div class="card-header-action">
								@if($classRoom->code)
								<form action="{{ route('class-room-code.delete') }}" method="post">@csrf
									<input type="hidden" name="class_room_id" value="{{ $classRoom->id }}">
									<button class="btn btn-icon icon-left btn-danger"><i class="far fa-trash"></i> Delete Code</button>
								</form>
								@else
								<form action="{{ route('class-room-code.generate') }}" method="post">@csrf
									<input type="hidden" name="class_room_id" value="{{ $classRoom->id }}">
									<button class="btn btn-icon icon-left btn-success"><i class="far fa-shere"></i> Generate Code</button>
								</form>
								@endif
							</div>
						</div>
						@if($classRoom->code)
						<div class="card-body">
							<h3>{{ $classRoom->code }}</h3>
							<p class="mb-0">Expired At: {{ $classRoom->code_expired_at->format('y-m-d H:i') }}</p>
						</div>
						@endif
					</div>
				</div>
				@endif
			</div>
			<div class="row">
				<div class="col-lg-6">
					<div class="card card-statistic-1">
						<a href="{{ route('class-rooms.students.index', $classRoom->slug) }}" class="d-block card-icon bg-primary"><i class="far fa-user m-0"></i></a>
						<div class="card-wrap">
							<div class="card-header"><h4>Total Students</h4></div>
							<div class="card-body">{{ $classRoom->students()->count() }}</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="card card-statistic-1">
						<a href="{{ route('class-rooms.posts.index', $classRoom->slug) }}" class="d-block card-icon bg-warning"><i class="far fa-newspaper m-0"></i></a>
						<div class="card-wrap">
							<div class="card-header"><h4>Total Posts</h4></div>
							<div class="card-body">{{ $classRoom->posts()->count() }}</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="card card-statistic-1">
						<a href="{{ route('class-rooms.resources.index', $classRoom->slug) }}" class="d-block card-icon bg-danger"><i class="fas fa-book-open m-0"></i></a>
						<div class="card-wrap">
							<div class="card-header"><h4>Total Resources</h4></div>
							<div class="card-body">{{ $classRoom->resources()->count() }}</div>
						</div>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="card card-statistic-1">
						<a href="{{ route('class-rooms.tasks.index', $classRoom->slug) }}" class="d-block card-icon bg-success"><i class="fas fa-tasks m-0"></i></a>
						<div class="card-wrap">
							<div class="card-header"><h4>Total Tasks / Exam</h4></div>
							<div class="card-body">{{ $classRoom->tasks()->count() }}</div>
						</div>
					</div>
				</div>
				@if(auth()->user()->role == 'teacher')
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header"><h4>Tasks Need to be Validate</h4></div>
						<div class="card-body p-0">
							<div class="table-responsive">
								<table class="table table-striped table-md">
									<thead>
										<tr>
											<th>Tasks / Exam</th>
											<th>Applied</th>
											<th>Scored</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($classRoom->tasks()->unscored()->get() as $task)
											<tr>
												<td>{{ $task->title }}</td>
												<td>{{ $task->submitedStudents()->count() }}</td>
												<td>{{ $task->submitedStudents()->scored()->count() }}</td>
												<td>
													<a href="{{ route('class-rooms.tasks.detail', [$classRoom->slug, $task->slug]) }}" class="btn btn-sm btn-icon icon-left btn-info"><i class="fas fa-eye"></i> Detail</a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				@else
				<div class="col-lg-12">
					<div class="card">
						<div class="card-header"><h4>Tasks Need to be Submited</h4></div>
						<div class="card-body p-0">
							<div class="table-responsive">
								<table class="table table-striped table-md">
									<thead>
										<tr>
											<th>Tasks / Exam</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
										@foreach($classRoom->tasks()->unsubmited()->get() as $task)
											<tr>
												<td>{{ $task->title }}</td>
												<td>
													<a href="{{ route('class-rooms.tasks.detail', [$classRoom->slug, $task->slug]) }}" class="btn btn-sm btn-icon icon-left btn-info"><i class="fas fa-upload"></i> Submit</a>
												</td>
											</tr>
										@endforeach
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				@endif
			</div>
		</div>
		<div class="col-lg-5">
			<article class="article article-style-c rounded overflow-hidden">
				<div class="article-header">
					<div class="article-image" data-background="{{ $classRoom->cover }}" style="background-image: url('{{ $classRoom->cover }}');"></div>
				</div>
				<div class="article-details">
					<div class="article-category"><a>{{ $classRoom->teacher->name }}</a></div>
					<div class="article-title"><h5 class="text-primary">{{ $classRoom->name }}</h5></div>
					<div class="mt-4">
						{!! $classRoom->description !!}
					</div>
					@if(auth()->user()->role == 'teacher')
					<div class="mt-4 text-center">
						<a href="{{ route('class-rooms.edit', $classRoom->slug) }}" class="btn btn-icon icon-left btn-warning"><i class="far fa-edit"></i> Edit Class Room Detail</a>	
						<button type="button" class="btn btn-icon icon-left btn-danger" data-confirm="Archive Class Room?|You are about to archived a class room. Archived class room will be hidden and only can be accessed through Home Page. Do you still want to continue?" data-confirm-yes="$('#archiveForm').submit();"><i class="fas fa-archive"></i> Archive Class Room</button>	
						<form id="archiveForm" action="{{ route('class-rooms.archive', $classRoom->slug) }}" method="post" class="hidden">@csrf @method('delete')</form>
					</div>
					@endif	
				</div>
			</article>
		</div>
	</div>
</x-app-layout>