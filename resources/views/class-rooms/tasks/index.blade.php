<x-app-layout>
	<x-slot name="header">{{ $classRoom->name }} Tasks</x-slot>

	<div class="row">
		@foreach($classRoom->tasks as $task)
		<div class="col-sm-6 col-lg-4 mb-4">
			<div class="card card-primary h-100">
				<div class="card-body d-flex flex-column align-items-stretch justify-content-between">
					<div>
						<h6 class="text-primary mb-4">{{ $task->title }}</h6>
						<table class="w-100">
							<tr class="">
								<td style="width: 2rem;"><i class="fas fa-user-clock"></i></td>
								<td>Created At</td>
								<td class="text-right">{{ $task->created_at->diffForHumans() }}</td>
							</tr>
							<tr class="">
								<td style="width: 2rem;"><i class="fas fa-calendar-times"></i></td>
								<td>Due At</td>
								<td class="text-right">{{ $task->deadline->diffForHumans() }}</td>
							</tr>
						</table>
					</div>
					<div>
						<a href="{{ route('class-rooms.tasks.detail', [$classRoom->slug, $task->slug]) }}" role="button" class="btn btn-sm btn-block btn-icon icon-left btn-outline-primary"><i class="fas fa-eye"></i> Show</a>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		@if(auth()->user()->roleIs('teacher'))
		<div class="col-sm-6 col-lg-4 mb-4">
			<div class="card bg-transparent border shadow-none h-100">
				<div class="card-body d-flex align-items-center justify-content-center">
					<a href="{{ route('class-rooms.tasks.create', $classRoom->slug) }}" role="button" class="btn btn-secondary btn-icon icon-left"><i class="fas fa-plus"></i> Add Task</a>
				</div>
			</div>
		</div>
		@endif
	</div>
</x-app-layout>