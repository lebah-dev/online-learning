<x-app-layout>
	<x-slot name="header">{{ $task->title }}</x-slot>

	<div class="row">
		<div class="col-lg-7">
			<div class="card">
				<div class="card-header">
					<h4 class="card-title">{{ $task->title }}</h4>
					@if(auth()->user()->roleIs('teacher'))
					<div class="card-header-action">
						<form id="deleteTask" action="{{ route('class-rooms.tasks.delete', [$classRoom->slug, $task->slug]) }}" method="post">
							@csrf @method('delete')
							<a href="{{ route('class-rooms.tasks.edit', [$classRoom->slug, $task->slug]) }}" class="btn btn-warning btn-icon"><i class="fas fa-edit"></i></a>
							<button type="button" class="btn btn-danger btn-icon" data-confirm="Delete Task?|You are about to delete a task. This action can not be undone. Do you still want to continue?" data-confirm-yes="$('#deleteTask').submit();"><i class="fas fa-trash"></i></button>
						</form>
					</div>
					@endif
				</div>
				<div class="card-body">
					<p class="text-muted text-small">
						<span class="mr-2"><i class="fas fa-calendar-times mr-1"></i> Due in {{ $task->deadline->format('d-m-Y H:i') }}</span>
						<span class="mr-2"><i class="fas fa-user-clock mr-1"></i> Created at {{ $task->created_at->format('d-m-Y H:i') }}</span>
					</p>
					<div>
						{!! $task->description !!}
					</div>
					<hr>
					<a href="{{ $task->attachment }}" target="_blank" class="btn btn-primary btn-icon icon-left"><i class="fas fa-file-download"></i> Attachment</a>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			@if(auth()->user()->roleIs('student'))
				@if($studentTask = auth()->user()->task($task->id))
				<div class="card mb-4">
					<div class="card-body">
						<div class="py-4 d-flex align-items-center justify-content-center">
							<a href="{{ $studentTask->attachment }}" class="btn btn-lg btn-info btn-icon icon-left"><i class="fas fa-file-download"></i> Submited Document</a>
						</div>
					</div>
				</div>
				@else
				
				<div class="card mb-4">
					<form action="{{ route('class-rooms.tasks.submit', [$classRoom->slug, $task->slug]) }}" method="post" enctype="multipart/form-data">
						@csrf
						<div class="card-body">
							<div class="d-flex align-items-stretch justify-content-between">
								<div class="form-group d-flex align-items-center m-0 mr-2">
									<input type="file" class="form-control-file" name="attachment">
								</div>
								<button class="btn btn-success btn-icon icon-left px-4"><i class="fas fa-file-upload"></i> Submit</button>
							</div>
							@error('attachment') <div class="text-small text-danger">{{ $message }}</div> @enderror
						</div>
					</form>
				</div>
				@endif
			@endif
			<div class="card mb-4">
				<div class="card-header">
					<h4 class="card-title">Students</h4>
					<div class="card-header-action">
						<span class="badge badge-info">Submited: {{ $task->submitedStudents()->count() }}</span>
						<span class="badge badge-success">Scored: {{ $task->submitedStudents()->whereNotNull('score')->count() }}</span>
					</div>
				</div>
			</div>

			<div class="card mb-2">
				<div class="card-body">
					<ul class="my-2 list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
						@forelse($task->submitedStudents as $submited)
						<li class="media align-items-center">
							<img alt="image" class="mr-3 rounded-circle" height="50" width="50" src="{{ $submited->student->avatar }}" style="object-fit: cover; object-position: center;">
							<div class="media-body">
								<div class="media-title">{{ $submited->student->name }}</div>
							</div>
							<div class="media-cta text-nowrap">
								<button type="button" class="btn btn-info btn-icon" title="See Student Detail" data-toggle="modal" data-target="#detail{{ $submited->student->id }}"><i class="fas fa-user"></i></button>
								<button type="button" class="btn btn-success btn-icon" title="See Submited Document" data-toggle="modal" data-target="#appliedDocument{{ $submited->student->id }}"><i class="fas fa-medal"></i> <span class="font-weight-bold">{{ $submited->score }}</span></button>
							</div>
	
							@if(auth()->user()->roleIs('teacher'))
							@push('modals')
							<x-user-modal :student="$submited->student" :class-room="$classRoom"></x-user-modal>

							<div class="modal fade" tabindex="-1" role="dialog" id="appliedDocument{{ $submited->student->id }}">
								<div class="modal-dialog" role="document">
									<form action="{{ route('class-rooms.tasks.score', [$classRoom->slug, $task->slug]) }}" method="post">
										@csrf
										<input type="hidden" name="student_id" value="{{ $submited->student->id }}">
										<div class="modal-content">
											<div class="modal-header">
												<h5 class="modal-title">{{ $submited->student->name }}</h5>
												<button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												</button>
											</div>
											<div class="modal-body">
												<div class="py-4 d-flex align-items-center justify-content-center rounded border border-light">
													<a href="{{ $submited->attachment }}" class="btn btn-lg btn-info btn-icon icon-left"><i class="fas fa-file-download"></i> Submited Document</a>
												</div>
											</div>
											<div class="px-4 py-3 modal-footer bg-whitesmoke">
												<div class="form-group m-0 w-100">
													<div class="input-group m-0">
														<input type="number" name="score" value="{{ old('score') ?? $submited->score }}" class="form-control @error('score') is-invalid @enderror" placeholder="0">
														<div class="input-group-append">
															<button class="btn btn-primary" type="submit">Update Score</button>
															<button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
							@endpush
							@endif
						</li>
						@empty
						<li class="media">
							<div class="media-body">
								<div class="media-title">No student submited yet</div>
							</div>
						</li>
						@endforelse
					</ul>
				</div>
			</div>
		</div>
	</div>
</x-app-layout>