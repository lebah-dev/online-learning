<x-app-layout>
	<x-slot name="header">Edit Class Room Tasks</x-slot>

	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<form action="{{ route('class-rooms.tasks.update', [$classRoom->slug, $task->slug]) }}" method="post" enctype="multipart/form-data" autocomplete="off">
					@csrf

					<div class="card-header">
						<h4>Class Room's Tasks Form</h4>
					</div>
					<div class="card-body">
						<div class="form-group">
							<label>Title</label>
							<input id="title" type="text" name="title" class="form-control @error('title') is-invalid @enderror" value="{{ old('title') ?? $task->title }}" onkeyup="$('#slug').val(slugify($(this).val()))">
							@error('title') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label>Slug</label>
							<input id="slug" type="text" name="slug" class="form-control @error('slug') is-invalid @enderror" value="{{ old('slug') ?? $task->slug }}">
							@error('slug') <div class="invalid-feedback">{{ $message }}</div> @enderror
							<div class="text-small text-muted">Used to identify the tasks. It must be unique.</div>
						</div>
						<div class="form-group mb-0">
							<label>Description</label>
							<textarea name="description" class="form-control summernote @error('description') is-invalid @enderror" rows="10">{{ old('description') ?? $task->description }}</textarea>
							@error('description') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
						<div class="form-group">
							<label>Attachment</label>
							<input type="file" name="attachment" value="{{ old('attachment') }}" class="form-control @error('attachment') is-invalid @enderror">
							@error('attachment') <div class="invalid-feedback">{{ $message }}</div> @enderror
							<p class="m-0 text-sm text-muted">Leave it empty if don't want to change <a href="{{ $task->attachment }}">current attachment.</a></p>
						</div>
						<div class="form-group">
							<label>Due Date</label>
							<input type="text" name="deadline" class="form-control datetimepicker @error('deadline') is-invalid @enderror" value="{{ old('deadline') ?? $task->deadline->format('Y-m-d\TH:i') }}">
							@error('deadline') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="card-footer text-right">
						<button class="btn btn-icon icon-left btn-primary"><i class="fas fa-save"></i> Save</button>
						<a href="{{ route('class-rooms.tasks.detail', [$classRoom->slug, $task->slug]) }}" class="btn btn-icon icon-left btn-secondary"><i class="fas fa-chevron-left"></i> Kembali</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	@push('css-libraries')
		<link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
		<link rel="stylesheet" href="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.css') }}">
	@endpush

	@push('js-libraries')
		<script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
		<script src="{{ asset('assets/modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
	@endpush
</x-app-layout>