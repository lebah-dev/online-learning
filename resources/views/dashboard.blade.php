<x-app-layout>
	<x-slot name="header">Welcome, {{ auth()->user()->name }}</x-slot>
	<div class="row">
		@foreach($classRooms as $classRoom)
		<div class="col-lg-3 mb-4">
			<div class="card card-primary h-100">
				<div class="card-body d-flex flex-column align-items-stretch justify-content-between">
					<div>
						<h6 class="text-primary mb-4">{{ $classRoom->name }}</h6>
						<p class="mb-0"><i class="fas fa-users"></i> Students : {{ $classRoom->students()->count() }}</p>
						<p class="mb-0"><i class="fas fa-newspaper"></i> Posts : {{ $classRoom->posts()->count() }}</p>
						<p class="mb-0"><i class="fas fa-book-open"></i> Resources : {{ $classRoom->resources()->count() }}</p>
						<p class="mb-0"><i class="fas fa-tasks"></i> Task / Exam : {{ $classRoom->tasks()->count() }}</p>
					</div>
					<div>
						<a href="{{ route('class-rooms.detail', $classRoom->slug) }}" role="button" class="btn btn-sm btn-block btn-icon icon-left btn-outline-primary"><i class="fas fa-eye"></i> Show</a>
					</div>
				</div>
			</div>
		</div>
		@endforeach
		
		<div class="col-lg-3 mb-4">
			<div class="card bg-transparent h-100 shadow-none border">
				<div class="card-body d-flex flex-column align-items-stretch justify-content-center">
					@if(auth()->user()->role == 'teacher')
					<a href="{{ route('class-rooms.create') }}" role="button" class="btn btn-block btn-secondary btn-icon icon-left"><i class="fas fa-plus"></i> Create New</a>
					<a href="{{ route('class-rooms.archived') }}" role="button" class="btn btn-block btn-secondary btn-icon icon-left"><i class="fas fa-archive"></i> See Archived</a>
					@else
					<button type="button" class="btn btn-block btn-secondary btn-icon icon-left" data-toggle="modal" data-target="#joinClassRoom"><i class="fas fa-plus"></i> Join Class Room</button>
					@endif
				</div>
			</div>
		</div>
	</div>

	@push('modals')
	<div class="modal fade" tabindex="-1" role="dialog" id="joinClassRoom">
		<form action="{{ route('class-room-code.join') }}" method="post">@csrf
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Join Class Room</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="form-group">
							<label>Class Room Code</label>
							<input type="text" name="code" value="{{ old('code') }}" placeholder="XXXXXX" class="form-control @error('code') is-invalid @enderror">
							@error('code') <div class="invalid-feedback">{{ $message }}</div> @enderror
						</div>
					</div>
					<div class="modal-footer bg-whitesmoke br">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Join</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	@endpush

	@error('code')
	@push('scripts')
	<script>
		$(document).ready(function () {
			$('#joinClassRoom').modal('show');
		})
	</script>
	@endpush
	@enderror
</x-app-layout>