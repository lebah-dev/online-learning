<x-app-layout>
	<x-slot name="header">Profile</x-slot>

	<div class="row mt-sm-4">
		<div class="col-12 col-md-12 col-lg-5">
			<div class="card profile-widget">
				<div class="profile-widget-header">
					<img alt="image" src="{{ auth()->user()->avatar }}" class="rounded-circle profile-widget-picture" style="width: 100px; height: 100px; object-fit: cover; object-position: center;">
					<div class="profile-widget-items">
						@if(auth()->user()->roleIs('teacher'))
						<div class="profile-widget-item">
							<div class="profile-widget-item-label">Owned Class Rooms</div>
							<div class="profile-widget-item-value">{{ auth()->user()->ownedClassRoom()->count() }}</div>
						</div>
						@else
						<div class="profile-widget-item">
							<div class="profile-widget-item-label">Joined Class Rooms</div>
							<div class="profile-widget-item-value">{{ auth()->user()->classRooms()->count() }}</div>
						</div>
						@endif
					</div>
				</div>
				<div class="profile-widget-description">
					<div class="profile-widget-name">{{ auth()->user()->name }} <div class="text-muted d-inline font-weight-normal text-capitalize">
							<div class="slash"></div> {{ auth()->user()->role }}
						</div>
					</div>
					<div>{!! auth()->user()->bio !!}</div>
				</div>
			</div>
		</div>
		<div class="col-12 col-md-12 col-lg-7">
			<div class="card">
				<form action="{{ route('profile.update') }}" method="post" class="needs-validation" novalidate="" enctype="multipart/form-data">
					@csrf
					<div class="card-header">
						<h4>Edit Profile</h4>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="form-group col-md-6 col-12">
								<label>Full Name</label>
								<input type="text" name="name" class="form-control" value="{{ auth()->user()->name }}" required="">
								@error('name')<div class="invalid-feedback">{{ $message }}</div>@enderror
							</div>
							<div class="form-group col-md-6 col-12">
								<label>Email</label>
								<input type="email" name="email" class="form-control" value="{{ auth()->user()->email }}" required="">
								@error('email')<div class="invalid-feedback">{{ $message }}</div>@enderror
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>Bio</label>
								<textarea class="form-control summernote-simple" name="bio">{{ auth()->user()->bio }}</textarea>
							</div>
						</div>
						<div class="row">
							<div class="form-group col-12">
								<label>Avatar</label>
								<input type="file" name="avatar" class="form-control">
								<div class="text-small text-muted">Leave it blank if don't want to update your avatar.</div>
							</div>
						</div>
					</div>
					<div class="card-footer text-right">
						<button class="btn btn-primary">Save Changes</button>
						<a href="{{ route('password.request') }}" class="btn btn-secondary">Reset Password</a>
					</div>
				</form>
			</div>
		</div>
	</div>

	@push('css-libraries')
	<link rel="stylesheet" href="{{ asset('assets/modules/summernote/summernote-bs4.css') }}">
	@endpush

	@push('js-libraries')
	<script src="{{ asset('assets/modules/summernote/summernote-bs4.js') }}"></script>
	@endpush
</x-app-layout>