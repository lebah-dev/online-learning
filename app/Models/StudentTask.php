<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudentTask extends Model
{
	const ATTACHMENT_PATH = 'class-rooms/tasks/submited-documents';

	protected $table = 'student_task';
	protected $fillable = [
		'class_room_id', 
		'student_id',
		'task_id',
		'attachment_path',
		'score'
	];

	public function student()
	{
		return $this->belongsTo(User::class, 'student_id');
	}

	public function getAttachmentAttribute()
	{
		return route('download-storage', encrypt($this->attachment_path));
	}

	public function scopeScored($query)
	{
		return $query->whereNotNull('score');
	}

	public function scopeUnscored($query)
	{
		return $query->whereNull('score');
	}
}