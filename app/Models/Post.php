<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
	protected $table = 'posts';
	protected $fillable = [
		'class_room_id',
		'title',
		'slug',
		'content',
		'published_at'
	];
	protected $casts = [
		'published_at' => 'datetime'
	];

	public function classRoom()
	{
		return $this->belongsTo(ClassRoom::class, 'class_room_id');
	}

	public function getContentPreviewAttribute()
	{
		return substr(strip_tags($this->content), 0, 100) . '...';
	}
}