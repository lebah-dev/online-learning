<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	const ATTACHMENT_PATH = 'class-rooms/tasks';

	protected $table = 'tasks';
	protected $fillable = [
		'class_room_id',
		'title',
		'slug',
		'description',
		'attachment_path',
		'deadline'
	];
	protected $casts = [
		'deadline' => 'datetime'
	];

	public function classRoom()
	{
		return $this->belongsTo(ClassRoom::class, 'class_room_id');
	}

	public function submitedStudents()
	{
		return $this->hasMany(StudentTask::class, 'task_id');
	}

	public function scopeSlug(Builder $query, $slug)
	{
		return $query->where('slug', $slug);
	}

	public function scopeUnsubmited(Builder $query)
	{
		return $query->whereDoesntHave('submitedStudents', function ($submitedStudents) {
			$submitedStudents->where('student_id', auth()->id());
		});
	}

	public function scopeUnscored(Builder $query)
	{
		return $query->whereHas('submitedStudents', function ($submitedStudents) {
			$submitedStudents->unscored();
		});
	}

	public function getAttachmentAttribute()
	{
		return route('download-storage', encrypt($this->attachment_path));
	}
}