<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassRoomStudent extends Model
{
	protected $table = 'class_room_student';
	protected $fillable = [
		'class_room_id', 'student_id', 'average_score'
	];

	public function classRoom()
	{
		return $this->belongsTo(ClassRoom::class, 'class_room_id');
	}

	public function student()
	{
		return $this->belongsTo(User::class, 'student_id')->where('role', User::ROLE_STUDENT);
	}

	public function getAverageScoreAttribute()
	{
		return round($this->average_score, 0);
	}
}