<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    const AVATAR_PATH = 'avatar';
    const ROLE_STUDENT = 'student';
    const ROLE_TEACHER = 'teacher';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
        'avatar_path',
        'bio',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function classRooms()
    {
        return $this->belongsToMany(ClassRoom::class, 'class_room_student', 'student_id', 'class_room_id')
            ->withPivot(['average_score']);
    }

    public function ownedClassRoom()
    {
        return $this->hasMany(ClassRoom::class, 'teacher_id');
    }

    public function classRoomStudent()
    {
        return $this->hasmany(ClassRoomStudent::class, 'student_id');
    }

    public function submitedTasks()
    {
        return $this->hasMany(StudentTask::class, 'student_id');
    }

    public function getAvatarAttribute()
    {
        return $this->avatar_path
            ? route('retrive-storage', encrypt($this->avatar_path))
            : 'https://ui-avatars.com/api/?name='.urlencode($this->name).'&color=7F9CF5&background=EBF4FF';
    }

    public function scopeSort($query, $request)
    {
        switch ($request->sort) {
            case 'student_name':
                $query->orderBy('name', $request->order);
                break;
            case 'average_score':
                $query->orderBy('class_room_student.average_score', $request->order);
                break;
            default:
                break;
        }

        return $query;
    }
    
    public function roleIs($role)
    {
        return $this->role == $role;
    }

    public function task($taskId)
    {
        return $this->submitedTasks()->where('task_id', $taskId)->first();
    }

    public function averageScore($classRoomId)
    {
        $classRoom = $this->classRooms()->find($classRoomId);
        return $classRoom->pivot->average_score ?? $this->updateAverageScore($classRoomId);
    }

    public function updateAverageScore($classRoomId)
    {
        $classRoom = $this->classRooms()->find($classRoomId);

        if ($classRoom && $classRoom->tasks()->count() > 0) {
            $classRoom->pivot->average_score = ($this->submitedTasks()->where('class_room_id', $classRoom->id)->sum('score') / ($classRoom->tasks()->count() * 100)) * 100;
            $classRoom->pivot->save();
        }

        return $classRoom->pivot->averageScore ?? '-';
    }
}
