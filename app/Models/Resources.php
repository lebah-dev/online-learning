<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resources extends Model
{
	const FILE_PATH = 'class-rooms/resources';
	protected $table = "resources";
	protected $fillable = [
		'class_room_id',
		'title',
		'slug',
		'type',
		'file_path',
	];

	public function classRoom()
	{
		return $this->belongsTo(ClassRoom::class, 'class_room_id');
	}

	public function scopeSlug($query, $slug)
	{
		return $query->where('slug', $slug);
	}

	public function getFileAttribute()
	{
		return route('retrive-storage', encrypt($this->file_path));
	}
}