<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassRoom extends Model
{
	const COVER_PATH = 'class_rooms/covers';

	protected $table = 'class_rooms';
	protected $fillable = [
		'teacher_id',
		'cover_path',
		'name',
		'slug',
		'description',
		'code',
		'code_expired_at',
		'archived_at'
	];
	protected $casts = [
		'code_expired_at' => 'datetime'
	];

	public function teacher()
	{
		return $this->belongsTo(User::class, 'teacher_id', 'id')
			->where('role', User::ROLE_TEACHER);
	}

	public function students()
	{
		return $this->belongsToMany(User::class, 'class_room_student', 'class_room_id', 'student_id')
			->withPivot(['average_score']);
	}

	public function posts()
	{
		return $this->hasMany(Post::class, 'class_room_id');
	}

	public function resources()
	{
		return $this->hasMany(Resources::class, 'class_room_id');
	}

	public function tasks()
	{
		return $this->hasMany(Task::class, 'class_room_id');
	}

	public function scopeMine($query, $user)
	{
		if ($user->role == User::ROLE_TEACHER) {
			$query->where('teacher_id', $user->id);
		} else {
			$query->whereHas('students', function ($students) use ($user) {
				$students->where('student_id', $user->id);
			});
		}

		return $query;
	}

	public function scopeSlug($query, $role)
	{
		return $query->where('slug', $role);
	}

	public function getCoverAttribute()
	{
		return route('retrive-storage', encrypt($this->cover_path));
	}
}