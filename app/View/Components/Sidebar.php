<?php

namespace App\View\Components;

use App\Models\ClassRoom;
use App\Models\User;
use Illuminate\View\Component;

class Sidebar extends Component
{
	public function render()
    {
        return view('components.sidebar');
	}
	
	public function classRooms()
	{
		return ClassRoom::query()
			->when(auth()->user()->role == User::ROLE_TEACHER, function ($query) {
				$query->where('teacher_id', auth()->id());
			})
			->when(auth()->user()->role == User::ROLE_STUDENT, function ($query) {
				$query->whereHas('students', function ($students) {
					$students->where('student_id', auth()->id());
				});
			})
			->whereNull('archived_at')
			->orderBy('name')
			->get();
	}
}