<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\Resources;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClassRoomResourceController extends Controller
{
	public function index(string $classRoomSlug)
	{
		return view('class-rooms.resources.index', [
			'classRoom' => ClassRoom::mine(auth()->user())
				->slug($classRoomSlug)
				->firstOrFail()
		]);
	}

	public function create(Request $request, string $classRoomSlug)
	{
		$request->validate([
			'title' => 'required',
			'slug' => 'required|unique:resources,slug',
			'file' => 'required|mimes:pdf'
		]);

		$classRoom = ClassRoom::mine(auth()->user())->slug($classRoomSlug)->firstOrFail();

		$resource = new Resources($request->only(['title', 'slug']));
		$resource->type = 'pdf';
		$resource->class_room_id = $classRoom->id;
		$resource->file_path = $request->file('file')->store(Resources::FILE_PATH);
		$resource->save();

		return redirect()->route('class-rooms.resources.index', $classRoomSlug);
	}

	public function detail(string $classRoomSlug, string $resourceSlug)
	{
		return view('class-rooms.resources.detail', [
			'classRoom' => $classRoom = ClassRoom::mine(auth()->user())->slug($classRoomSlug)->firstOrFail(),
			'resource' => $classRoom->resources()->slug($resourceSlug)->firstOrFail(),
		]);
	}

	public function delete(string $classRoomSlug, string $resourceSlug)
	{
		$classRoom = ClassRoom::mine(auth()->user())->slug($classRoomSlug)->firstOrFail();
		$resource = $classRoom->resources()->slug($resourceSlug)->firstOrFail();

		Storage::delete($resource->file_path);
		$resource->delete();

		return redirect()->route('class-rooms.resources.index', $classRoomSlug);
	}
}