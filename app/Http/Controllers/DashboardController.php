<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\User;

class DashboardController extends Controller
{
    public function index()
    {
        return view('dashboard', [
            'classRooms' => ClassRoom::mine(auth()->user())
                ->whereNull('archived_at')
                ->orderBy('name')
                ->get()
        ]);
    }
}