<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\StudentTask;
use App\Models\Task;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;

class ClassRoomTaskController extends Controller
{
	private $classRoom;
	private $task;

	public function __construct()
	{
		$this->classRoom = ClassRoom::slug(Request::route('classRoomSlug'))->firstOrFail();
		$this->task = Request::route('taskSlug')
			? $this->classRoom->tasks()->slug(Request::route('taskSlug'))->firstOrFail()
			: null;
	}

	public function index()
	{
		return view('class-rooms.tasks.index', [
			'classRoom' => $this->classRoom,
		]);
	}

	public function create()
	{
		return view('class-rooms.tasks.create', [
			'classRoom' => $this->classRoom,
		]);
	}

	public function store()
	{
		Request::validate([
			'title' => 'required',
			'slug' => 'required|unique:tasks,slug',
			'description' => 'required',
			'attachment' => 'nullable|file',
			'deadline' => 'required|date|after:now'
		]);

		$task = new Task(Request::all(['title', 'slug', 'description', 'deadline']));
		if (Request::has('attachment')) {
			$task->attachment_path = Request::file('attachment')->store(Task::ATTACHMENT_PATH);
		}
		$task->class_room_id = $this->classRoom->id;
		$task->save();

		return Redirect::route('class-rooms.tasks.index', $this->classRoom->slug);
	}

	public function detail()
	{
		return view('class-rooms.tasks.detail', [
			'classRoom' => $this->classRoom,
			'task' => $this->task,
		]);
	}

	public function edit()
	{
		return view('class-rooms.tasks.edit', [
			'classRoom' => $this->classRoom,
			'task' => $this->task,
		]);
	}

	public function update()
	{
		Request::validate([
			'title' => 'required',
			'slug' => 'required|unique:tasks,slug,' . $this->task->id,
			'description' => 'required',
			'attachment' => 'nullable|file',
			'deadline' => 'required|date|after:now'
		]);

		$this->task->fill(Request::all(['title', 'slug', 'description', 'deadline']));
		if (Request::has('attachment')) {
			Storage::delete($this->task->attachment_path);
			$this->task->attachment_path = Request::file('attachment')->store(Task::ATTACHMENT_PATH);
		}
		$this->task->save();

		return Redirect::route('class-rooms.tasks.detail', [$this->classRoom->slug, $this->task->slug]);
	}

	public function delete()
	{
		Storage::delete($this->task->attachment_path);
		$this->task->delete();
		return Redirect::route('class-rooms.tasks.index', $this->classRoom->slug);
	}

	public function submit()
	{
		Request::validate([
			'attachment' => 'required|file'
		]);

		$studentTask = StudentTask::firstOrNew([
				'class_room_id' => $this->classRoom->id,
				'student_id' => auth()->id(),
				'task_id' => $this->task->id
			]);

		if ($studentTask->attachment_path) {
			Storage::delete($studentTask->attachment_path);
		}

		$studentTask->attachment_path = Request::file('attachment')->store(StudentTask::ATTACHMENT_PATH);
		$studentTask->score = null;
		$studentTask->save();

		return Redirect::route('class-rooms.tasks.detail', [$this->classRoom->slug, $this->task->slug]);
	}

	public function score()
	{
		$studentTask = StudentTask::query()
			->where('class_room_id', $this->classRoom->id)
			->where('student_id', Request::get('student_id'))
			->where('task_id', $this->task->id)
			->firstOrFail();
		
		$studentTask->score = Request::get('score');
		$studentTask->save();
		
		$studentTask->student->updateAverageScore($this->classRoom->id);

		return Redirect::route('class-rooms.tasks.detail', [$this->classRoom->slug, $this->task->slug]);
	}
}