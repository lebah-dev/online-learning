<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\ClassRoomStudent;
use App\Models\User;
use Illuminate\Http\Request;

class ClassRoomStudentController extends Controller
{
	public function index(string $classRoomSlug)
	{
		return view('class-rooms.students.index', [
			'classRoom' => ClassRoom::mine(auth()->user())
				->where('slug', $classRoomSlug)
				->firstOrFail(),
		]);
	}

	public function invite(Request $request, string $classRoomSlug)
	{
		$request->validate([
			'student_email' => 'required|exists:users,email'
		]);

		$user = User::query()
			->where('role', User::ROLE_STUDENT)
			->where('email', $request->student_email)
			->first();

		if (!$user) return redirect()->back()->withInput()->withErrors([
			'student_email' => 'There is no student with given email yet.'
		]);

		$classRoom = ClassRoom::mine(auth()->user())->where('slug', $classRoomSlug)->firstOrFail();
		if (!$classRoom->students()->find($user->id)) $classRoom->students()->attach([$user->id]);

		return redirect()->back();
	}

	public function detach(Request $request, string $classRoomSlug)
	{
		$classRoom = ClassRoom::mine(auth()->user())->where('slug', $classRoomSlug)->firstOrFail();
		$classRoom->students()->detach([$request->student_id]);

		return redirect()->back();
	}
}