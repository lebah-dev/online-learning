<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ClassRoomCodeController extends Controller
{
	public function generate(Request $request)
	{
		$request->validate([
			'class_room_id' => 'required|exists:class_rooms,id'
		]);

		$code = substr(md5(microtime()),rand(0,26),6);
		while (ClassRoom::query()->where('code', $code)->exists()) {
			$code = substr(md5(microtime()),rand(0,26),6);
		}

		ClassRoom::mine(auth()->user())
			->where('id', $request->class_room_id)
			->update([
				'code' => $code,
				'code_expired_at' => Carbon::now()->addDays(7)
			]);

		return redirect()->back();
	}

	public function delete(Request $request)
	{
		$request->validate([
			'class_room_id' => 'required|exists:class_rooms,id'
		]);

		ClassRoom::mine(auth()->user())
			->where('id', $request->class_room_id)
			->update([
				'code' => null,
				'code_expired_at' => null
			]);

		return redirect()->back();
	}

	public function join(Request $request)
	{
		$request->validate([
			'code' => 'required'
		]);

		$classRoom = ClassRoom::query()->where('code', $request->code)->where('code_expired_at', '>', now())->first();
		if (!$classRoom) return redirect()->back()->withErrors([
			'code' => 'Code is invalid. Please double check your code or ask your teacher.'
		]);

		if (!$classRoom->students()->find(auth()->id())) $classRoom->students()->attach(auth()->user());

		return redirect()->back();
	}
}