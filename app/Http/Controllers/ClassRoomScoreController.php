<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use Illuminate\Support\Facades\Request;

class ClassRoomScoreController extends Controller
{
	private $classRoom;

	public function __construct()
	{
		$this->classRoom = ClassRoom::slug(Request::route('classRoomSlug'))->firstOrFail();
	}

	public function index()
	{
		return view('class-rooms.score.index', [
			'classRoom' => $this->classRoom,
		]);
	}
}