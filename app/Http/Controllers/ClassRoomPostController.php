<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClassRoomPostController extends Controller
{
	public function index(string $classRoomSlug)
	{
		return view('class-rooms.posts.index', [
			'classRoom' => ClassRoom::mine(auth()->user())
				->slug($classRoomSlug)
				->firstOrFail()
		]);
	}

	public function create(string $classRoomSlug)
	{
		return view('class-rooms.posts.create', [
			'classRoom' => ClassRoom::mine(auth()->user())
				->slug($classRoomSlug)
				->firstOrFail()
		]);
	}

	public function store(Request $request, string $classRoomSlug)
	{
		$request->validate([
			'title' => 'required',
			'slug' => 'required|unique:posts,slug',
			'content' => 'required',
		]);

		$classRoom = ClassRoom::mine(auth()->user())
			->slug($classRoomSlug)->firstOrFail();

		$post = new Post($request->only(['title', 'slug', 'content']));
		$post->class_room_id = $classRoom->id;
		$post->published_at = now();
		$post->save();

		return redirect()->route('class-rooms.posts.index', $classRoomSlug);
	}

	public function detail(string $classRoomSlug, string $postSlug)
	{
		return view('class-rooms.posts.detail', [
			'classRoom' => $classRoom = ClassRoom::mine(auth()->user())
				->slug($classRoomSlug)->firstOrFail(),
			'post' => $classRoom->posts()->where('slug', $postSlug)->firstOrFail()
		]);
	}

	public function edit(string $classRoomSlug, string $postSlug)
	{
		return view('class-rooms.posts.edit', [
			'classRoom' => $classRoom = ClassRoom::mine(auth()->user())
				->slug($classRoomSlug)->firstOrFail(),
			'post' => $classRoom->posts()->where('slug', $postSlug)->firstOrFail()
		]);
	}

	public function update(Request $request, string $classRoomSlug, string $postSlug)
	{
		$classRoom = ClassRoom::mine(auth()->user())
			->slug($classRoomSlug)->firstOrFail();
		$post = $classRoom->posts()->where('slug', $postSlug)->firstOrFail();

		$request->validate([
			'title' => 'required',
			'slug' => 'required|unique:posts,slug,'.$post->id,
			'content' => 'required',
		]);

		$post->fill($request->only(['title', 'slug', 'content']));
		$post->class_room_id = $classRoom->id;
		$post->published_at = now();
		$post->save();

		return redirect()->route('class-rooms.posts.detail', [$classRoomSlug, $post->slug]);
	}

	public function delete(string $classRoomSlug, string $postSlug)
	{
		$classRoom = ClassRoom::mine(auth()->user())
			->slug($classRoomSlug)->firstOrFail();
		$post = $classRoom->posts()->where('slug', $postSlug)->firstOrFail();
		$post->delete();

		return redirect()->route('class-rooms.posts.index', $classRoomSlug);
	}
}