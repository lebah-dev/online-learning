<?php

namespace App\Http\Controllers;

use App\Models\ClassRoom;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ClassRoomController extends Controller
{
	public function store(Request $request)
	{
		$request->validate([
			'cover' => 'required|image',
			'name' => 'required',
			'slug' => 'required|unique:class_rooms,slug',
			'description' => 'required'
		]);

		$classRoom = new ClassRoom();
		$classRoom->fill($request->only(['name', 'slug', 'description']));
		$classRoom->teacher_id = auth()->id();
		$classRoom->cover_path = $request->file('cover')->store(ClassRoom::COVER_PATH);
		$classRoom->save();

		return redirect()->route('class-rooms.detail', $classRoom->slug);
	}

	public function detail(string $slug)
	{
		return view('class-rooms.detail', [
			'classRoom' => ClassRoom::mine(auth()->user())
				->where('slug', $slug)
				->firstOrFail()
		]);
	}

	public function edit(string $slug)
	{
		return view('class-rooms.edit', [
			'classRoom' => ClassRoom::mine(auth()->user())
				->where('teacher_id', auth()->id())
				->where('slug', $slug)->firstOrFail()
		]);
	}

	public function update(Request $request, string $slug)
	{
		$classRoom = ClassRoom::mine(auth()->user())
			->where('teacher_id', auth()->id())
			->where('slug', $slug)->firstOrFail();

		$request->validate([
			'cover' => 'nullable|image',
			'name' => 'required',
			'slug' => 'required|unique:class_rooms,slug,'.$classRoom->id,
			'description' => 'nullable'
		]);

		$classRoom->fill($request->only(['name', 'slug', 'description']));
		if ($request->file('cover')) {
			Storage::delete($classRoom->cover_path);
			$classRoom->cover_path = $request->file('cover')->store(ClassRoom::COVER_PATH);
		}

		$classRoom->save();

		return redirect()->route('class-rooms.detail', $classRoom->slug);
	}

	public function archive(string $slug)
	{
		ClassRoom::mine(auth()->user())->where('teacher_id', auth()->id())->where('slug', $slug)->update([
			'archived_at' => now()
		]);

		return redirect()->route('dashboard');
	}

	public function archived()
	{
		return view('class-rooms.archived', [
			'classRooms' => ClassRoom::mine(auth()->user())
				->where('teacher_id', auth()->id())
				->whereNotNull('archived_at')
				->orderBy('name')
				->get()
		]);
	}

	public function restore(string $slug)
	{
		ClassRoom::mine(auth()->user())
			->where('teacher_id', auth()->id())
			->where('slug', $slug)->update([
				'archived_at' => null
			]);

		return redirect()->route('class-rooms.archived');
	}
}