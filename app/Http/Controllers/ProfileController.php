<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;

class ProfileController extends Controller
{
	public function profile()
	{
		return view('profile');
	}

	public function update()
	{
		Request::validate([
			'name' => 'required',
			'email' => 'required|unique:users,email,'.auth()->id(),
			'bio' => 'nullable',
			'avatar' => 'nullable|image',
		]);

		$user = User::find(auth()->id());
		$user->fill(Request::all(['name', 'email', 'bio']));
		if (Request::has('avatar')) {
			Storage::delete($user->avatar_path);
			$user->avatar_path = Request::file('avatar')->store(User::AVATAR_PATH);
		}
		$user->save();

		return redirect()->back();
	}
}